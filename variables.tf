variable "region" {
  type        = string
  description = "AWS Region"
}

variable "private_subnet_cidr" {
  type        = string
  description = "CIDR for private subnet"
}

variable "availability_zone" {
  type        = string
  description = "AZ to create private subnet"
}

variable "availability_zone_2" {
  type        = string
  description = "AZ to create ALB"
}

variable "bucket_name" {
  type        = string
  description = "S3 bucket name"
}
