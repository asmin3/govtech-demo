output "lb_endpoint" {
  value = "http://${aws_lb.application_loadbalancer.dns_name}"
}

output "asg_name" {
  value = aws_autoscaling_group.fleet-a.name
}

output "s3_endpoint" {
  value = aws_s3_access_point.s3_access_point
}
