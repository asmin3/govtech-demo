region = "ap-southeast-1"

private_subnet_cidr = "172.31.64.0/20"

availability_zone = "ap-southeast-1a"

availability_zone_2 = "ap-southeast-1b"

bucket_name = "its-private-bucket"
