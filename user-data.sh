#!/bin/bash
sudo yum -y update
sudo amazon-linux-extras install nginx1 -y
sudo systemctl start nginx
sudo systemctl enable nginx
sudo aws s3 cp s3://its-private-bucket/index.html /usr/share/nginx/html/index.html --region ap-southeast-1
sudo systemctl reload nginx
