terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.33.0"
    }
  }
}

provider "aws" {
  region = var.region
}

## Networking Resource Creation

resource "aws_default_vpc" "default" {
}

data "aws_availability_zone" "az" {
  name = var.availability_zone
}

#private subnet creation

resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_default_vpc.default.id
  cidr_block        = var.private_subnet_cidr
  availability_zone = data.aws_availability_zone.az.id

  tags = {
    Name = "PrivateSubnet:${data.aws_availability_zone.az.id}"
  }
}

#public subnet filter

resource "aws_default_subnet" "default_az1" {
  availability_zone = var.availability_zone
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = var.availability_zone_2
}

#private route table

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_default_vpc.default.id

  tags = {
    Name = "PrivateRouteTable:${data.aws_availability_zone.az.id}"
  }
}

#private route table assocication with private subnet

resource "aws_route_table_association" "rtb-association" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_route_table.id

}

##S3 Bucket Creation

#Private S3 Bucket

resource "aws_s3_bucket" "private_bucket" {
  #bucket = "${var.bucket_name}"
  bucket = "${var.bucket_name}"
}

resource "aws_s3_bucket_public_access_block" "block_public_access" {
  bucket = aws_s3_bucket.private_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#Put Objects into S3

resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.private_bucket.bucket
  key    = "index.html"
  source = "files/index.html"
}

#S3 Access Point

resource "aws_s3_access_point" "s3_access_point" {
  bucket = aws_s3_bucket.private_bucket.bucket
  name   = "s3-access-point"

  vpc_configuration {
    vpc_id = aws_default_vpc.default.id
  }

  depends_on = [
    aws_s3_bucket.private_bucket
  ]
}

#VPC Endpoint for S3

resource "aws_vpc_endpoint" "vpc_s3_endpoint" {
  vpc_id          = aws_default_vpc.default.id
  service_name    = "com.amazonaws.${var.region}.s3"
  route_table_ids = [aws_route_table.private_route_table.id]

  tags = {
    Name = "vpc-s3-endpoint"
  }

  depends_on = [
    aws_s3_bucket.private_bucket,
    aws_s3_access_point.s3_access_point,
    aws_route_table.private_route_table
  ]
}

##IAM Policies and Roles Creation

#IAM Policy to access S3 Bucket

resource "aws_iam_policy" "ec2_s3_access_policy" {
  name        = "S3-Access-Policy"
  path        = "/"
  description = "EC2 Instances to access S3 bucket"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action": [ "s3:*" ],
        "Resource" : [
          "${aws_s3_bucket.private_bucket.arn}/*"
        ]
      }
    ]
  })
}

#IAM Role with above policy

resource "aws_iam_role" "ec2_s3_access_role" {
  name = "EC2-get-S3Bucket-Objects"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ec2.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

#Attach IAM Policy to Role

resource "aws_iam_policy_attachment" "attach_iam_policy" {
  name       = "iam_policy_attachment_role"
  roles      = [aws_iam_role.ec2_s3_access_role.name]
  policy_arn = aws_iam_policy.ec2_s3_access_policy.arn
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_fleet_iam_profile"
  role = aws_iam_role.ec2_s3_access_role.name
}

#SSH Key

resource "aws_key_pair" "ssh_key" {
  key_name   = "asmin-key"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPa2YZUZZKXwKBjGFhaCdBkcD364zaQMbuBJH6hoBYlA asmin@LAPTOP-8QGHKREE"
}

#AMI

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5*-x86_64-gp2"]
  }
}

##Security Groups

#ALB

resource "aws_security_group" "alb_sg" {
  name = "ALB-SG"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  


  vpc_id = aws_default_vpc.default.id
}


#EC2 Fleet

resource "aws_security_group" "ec2_fleet_sg" {
  name = "EC2-Fleet-SG"

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allow SSH from private subnet"
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.alb_sg.id]
  }
  

  vpc_id = aws_default_vpc.default.id

  depends_on = [
    aws_security_group.alb_sg,
    aws_s3_bucket.private_bucket
  ]
}

#AWS Launch Template

resource "aws_launch_template" "ec2_launch" {

  name = "ec2-fleet"

  disable_api_stop        = false
  disable_api_termination = false

  ebs_optimized = false

  image_id = data.aws_ami.amazon_linux.image_id

  instance_initiated_shutdown_behavior = "terminate"

  instance_type = "t2.micro"

  key_name = aws_key_pair.ssh_key.key_name

  user_data = filebase64("./user-data.sh")

  network_interfaces {
    subnet_id       = aws_subnet.private_subnet.id
    security_groups = [aws_security_group.ec2_fleet_sg.id]
  }

  iam_instance_profile {
    name = aws_iam_instance_profile.ec2_profile.name
  }

  metadata_options {
    http_endpoint          = "enabled"
    instance_metadata_tags = "enabled"
  }

  monitoring {
    enabled = false
  }

  private_dns_name_options {
    enable_resource_name_dns_a_record = true
    hostname_type                     = "ip-name"
  }
}

#AWS Auto Scaling Group

resource "aws_autoscaling_group" "fleet-a" {
  name                = "its-autoscaling-group"
  vpc_zone_identifier = [aws_subnet.private_subnet.id]
  desired_capacity    = 2
  max_size            = 3
  min_size            = 2

  target_group_arns = [aws_lb_target_group.alb_target_group.arn]

  launch_template {
    id      = aws_launch_template.ec2_launch.id
    version = "$Latest"
  }

  depends_on = [
    aws_s3_bucket.private_bucket
  ]
}

#ALB

resource "aws_lb" "application_loadbalancer" {
  name               = "its-loadbalancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
}

#ALB Listner

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.application_loadbalancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}

#ALB Target Group

resource "aws_lb_target_group" "alb_target_group" {
  name     = "alb-its-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_default_vpc.default.id
}

#Auto Scaling Attachment

resource "aws_autoscaling_attachment" "autoscaling_attachment" {
  autoscaling_group_name = aws_autoscaling_group.fleet-a.name
  lb_target_group_arn    = aws_lb_target_group.alb_target_group.arn
}


## Extra - Add IAM User,Policy to GitLab
